import './App.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Home from './pages/Home';
import './components/index.css';
import Coctails from './pages/Coctails';
import Contact from './pages/Contact';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/coctails" component={Coctails} />
        <Route path="/contact" component={Contact} />
      </Switch>
    </Router>
  );
}

export default App;
