import React from "react";
import HeroSection from "../components/HeroSection";
import ContentSection from "../components/ContentSection";
import ContactForm from "../components/ContactForm";
import Footer from '../components/Footer';

const Contact = () => {
  return (
    <>
      <div className="hero-contact">
        <HeroSection
          imgsrc={"/images/contact.jpg"}
          title={"Contact"}
          subtitle={"FOR ANYTHING , ANYTIME"}
        />
      </div>
      <ContentSection
        rowSubtitle={"ANYTIME , ANYDAY"}
        rowTitle={"REACH US"}
        dailyTitle={"LOCATION"}
        dailyImgscr={"/images/loc.png"}
        dailyDesc={"8th Avenue New York City"}
        dailyTitle1={"OPENING"}
        dailyImgscr1={"/images/clock.png"}
        dailyDesc1={"Friday - Saturday - Sunday 8 P.M"}
        dailyTitle2={"SETUP YOUR EVENT"}
        dailyImgscr2={"/images/icecream.png"}
        dailyDesc2={
          "Pick a day and organise your event. Reach us for more info"
        }
      />
      <ContactForm />
      <div className="contact-footer">
              <Footer />
      </div>

    </>
  );
};

export default Contact;
