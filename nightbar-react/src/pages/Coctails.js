import React from "react";
import HeroSection from "../components/HeroSection";
import ContentSection from "../components/ContentSection";
import CoctailsSection from "../components/CoctailsSection";
import BarSection from "../components/BarSection";
import Footer from "../components/Footer";

const Coctails = () => {
  return (
    <>
      <HeroSection
        imgsrc={"/images/coctail.jpg"}
        title={"Recipies"}
        subtitle={"Fresh awesomeness"}
      />
      <ContentSection
        rowSubtitle={"OUR SECRET COMPONENTS"}
        rowTitle={"A Daily escape"}
        dailyTitle={"FRESH LIQUOR"}
        dailyImgscr={"/images/liquir.png"}
        dailyDesc={
          "Called fifth so life was life likeness thing don`t meat lights heaven, forth replenish meat isn`t night don`t spirit fifth."
        }
        dailyTitle1={"RIPE FRUIT"}
        dailyImgscr1={"/images/strawberry.png"}
        dailyDesc1={
          "Also to signs. You brought living Fowl green face it after years gathered you`ll upon two. Above, god living, replenish."
        }
        dailyTitle2={"SECRET SKILLS"}
        dailyImgscr2={"/images/ice.png"}
        dailyDesc2={
          "Gathered forth, won`t you`ll fly bring seasons you`ll the in make. Them evening thing in whales great won`t there bring."
        }
      />
      <CoctailsSection />
      <BarSection />
      <div className="coctails-footer">
        <Footer />
      </div>
    </>
  );
};

export default Coctails;
