import React from "react";
import HeroSection from "../components/HeroSection";
import ContentSection from "../components/ContentSection";
import Article from "../components/Article";
import EventSection from "../components/EventsSection";
import Footer from "../components/Footer";

function Home() {
  return (
    <>
      <HeroSection
        imgsrc={"/images/hero.jpg"}
        title={"Night"}
        subtitle={"All day coctail bar"}
      />
      <ContentSection
        rowSubtitle={"Coctail bar"}
        rowTitle={"A Daily escape"}
        dailyTitle={"TEQUILA SUNRISE"}
        dailyImgscr={"/images/tequila.png"}
        dailyDesc={
          "Called fifth so life was life likeness thing don't meat lights heaven, forth replenish meat isn't night don't spirit fifth."
        }
        dailyTitle1={"MOJITO BAY"}
        dailyImgscr1={"/images/mohito.png"}
        dailyDesc1={
          "Also to signs. You brought living Fowl green face it after years gathered you'll upon two. Above, god living, replenish."
        }
        dailyTitle2={"BLOODY MARY"}
        dailyImgscr2={"/images/bloody.png"}
        dailyDesc2={
          "Gathered forth, won't you'll fly bring seasons you'll the in make. Them evening thing in whales great won't there bring."
        }
      />
      <Article />
      <EventSection />
      <Footer />
    </>
  );
}

export default Home;
