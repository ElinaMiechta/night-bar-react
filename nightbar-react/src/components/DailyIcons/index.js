import React from "react";

const DailyIcons = ({ title, imgsrc, desc}) => {
  return (
    <div className="daily-container">
      <img src={imgsrc} alt="drink" />
      <h2 className="daily__title">{title}</h2>
      <p>{desc}</p>
      <div id="gr"></div>
    </div>
  );
};

export default DailyIcons;
