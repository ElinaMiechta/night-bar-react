import React, { useEffect } from "react";
import { BiShapeTriangle } from "react-icons/bi";
import "./index.css";

const BarSection = () => {
  return (
    <section className="bar-wrapper">
      <img src="/images/coctail-1.jpg" alt="lounge" id="parallax" />
      <div className="section__content">
        <h2>NIGHT LOUNGE</h2>
        <BiShapeTriangle className="hero-logo" />
        <h3> An elegant place to spend your special evening</h3>
      </div>
    </section>
  );
};

export default BarSection;
