import React, { useEffect } from "react";
import { Link } from "react-router-dom";

const Nav = ({ handleClick }) => {
  const clickAnchor = elem => elem.scrollIntoView();

  useEffect(() => {
    const elem = document.querySelector(".events-container"),
      link = document.querySelector("#event");
    return elem
      ? link.addEventListener("click", () => clickAnchor(elem))
      : null;
  });

  return (
    <ul className="nav-list">
      <li className="nav-item">
        <Link to="/" className="nav-links" onClick={handleClick}>
          Home
        </Link>
      </li>
      <li className="nav-item">
        <Link to="/coctails" className="nav-links" onClick={handleClick}>
          Coctails
        </Link>
      </li>
      <li className="nav-item" id="event">
        Event
      </li>
      <li className="nav-item">
        <Link to="/contact" className="nav-links" onClick={handleClick}>
          Contact
        </Link>
      </li>
    </ul>
  );
};

export default Nav;
