import React, { useEffect } from "react";
import DailyIcons from "../DailyIcons";
import Row from "../Row";

const animate = () => {
  window.addEventListener("scroll", function() {
    const elem = document.querySelector(".col-3");
    if (elem.classList.contains("animate")) {
      return;
    }
    const doc = document.documentElement;
    const top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    if (top >= 485) {
      elem.className += " ";
      elem.className += "animate";
    }
  });
};

const ContentSection = ({
  rowSubtitle,
  rowTitle,
  dailyTitle,
  dailyImgscr,
  dailyDesc,
  dailyTitle1,
  dailyImgscr1,
  dailyDesc1,
  dailyTitle2,
  dailyImgscr2,
  dailyDesc2
}) => {
  useEffect(() => {
    animate();
  });

  return (
    <div className="daily-content">
      <Row subTitle={rowSubtitle} title={rowTitle} />
      <div className="col-3">
        <DailyIcons title={dailyTitle} imgsrc={dailyImgscr} desc={dailyDesc} />
        <DailyIcons
          title={dailyTitle1}
          imgsrc={dailyImgscr1}
          desc={dailyDesc1}
        />
        <DailyIcons
          title={dailyTitle2}
          imgsrc={dailyImgscr2}
          desc={dailyDesc2}
        />
      </div>
    </div>
  );
};

export default ContentSection;
