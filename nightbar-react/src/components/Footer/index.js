import React from 'react';
import './index.css';
import {AiFillInstagram} from 'react-icons/ai';
import {AiFillFacebook} from 'react-icons/ai';
import {AiFillTwitterSquare} from 'react-icons/ai';
import {FaPinterestSquare} from 'react-icons/fa';

const Footer = () => {
      return (
            <div className="footer-wrapper">
                  <div className="col-4">
                        <h3>OPENING</h3>
                        <p>Friday-Saturday-Sunday</p>
                        <p>8 P.M.</p>
                  </div>
                  <div className="col-5">
                  <h3>LOCATION</h3>
                  <p>8th Avenue</p>
                  <p>NYC</p>
                  </div>
                  <div className="social-icons">
                        <AiFillInstagram className="sm-icon"/>
                        <AiFillFacebook className="sm-icon"/>
                        <AiFillTwitterSquare className="sm-icon"/>
                        <FaPinterestSquare className="sm-icon"/>
                  </div>                
            </div>
      )
}

export default Footer
