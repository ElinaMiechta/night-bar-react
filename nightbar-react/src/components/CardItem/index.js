import React from "react";
import {BsGem} from 'react-icons/bs';
import './index.css';

const CardItem = ({ title, subtitle, imgsrc, hrefTxt }) => {
  return (
    <div className="cards-container">
      <img src={imgsrc} alt="drink" />
      <span>{subtitle}</span>
      <h2 className="daily__title">{title}</h2>
       <BsGem className="card-item__icon"/>
      <a to="#">{hrefTxt}</a>
    </div>
  );
};

export default CardItem;
