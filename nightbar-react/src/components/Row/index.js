import React from "react";
import {BiShapeTriangle} from 'react-icons/bi';

function Row({ subTitle, title }) {
  return (
    <div className="row">
      <span>{subTitle}</span>
      <h2>{title}</h2>
      <BiShapeTriangle className="hero-logo" />
    </div>
  );
}

export default Row;
