import React from "react";
import { BiUser } from "react-icons/bi";
import { AiOutlineMail } from "react-icons/ai";
import { BsPencil } from "react-icons/bs";
import "./index.css";

const ContactForm = () => {
  const handleSubmit = e => {
    e.preventDefault();
  };

  return (
    <div className="form-wrapper">
      <form className="contact-form" onSubmit={e => handleSubmit(e)}>
        <div className="col-name">
          <BiUser className="user-icon" />
          <input type="text" name="name" placeholder="Name" />
        </div>
        <div className="col-email">
          <AiOutlineMail className="user-icon" />
          <input type="email" name="email" placeholder="E-mail" />
        </div>
        <div className="col-msg">
          <BsPencil className="user-icon" />
          <textarea name="msg" placeholder="Your message" />
        </div>
        <p>* All fields are required</p>
        <button type="submit">Send</button>
      </form>
    </div>
  );
};

export default ContactForm;
