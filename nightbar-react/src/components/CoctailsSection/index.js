import React, { useState, useEffect } from "react";
import Row from "../Row";
import CardItem from "../CardItem";
import "./index.css";
import axios from "axios";

const CoctailsSection = () => {
  const [items, setItems] = useState([]);
  const [loaded, setLoaded] = useState(false);

  const fetchAPI = () => {
    return axios
      .get("https://www.thecocktaildb.com/api/json/v2/1/popular.php")
      .then(res => {
        setLoaded(true);
        let list = [...res.data.drinks];
        list.length = 10;
        setItems(list);
        items.length = 10;
      })
      .catch(error => {
        setLoaded(false);
      });
  };

  useEffect(() => {
    fetchAPI();
  }, []);

  return (
    <div className="coctails-section">
      <Row subTitle={"TOP LEVEL COCTAILS"} title={"FRESH COCKTAILS"} />
      <div className="cards">
        {loaded ? (
          items.map((item, index) => {
            return (
              <CardItem
                title={item.strDrink}
                subtitle={item.strIBA}
                imgsrc={item.strDrinkThumb}
                hrefTxt={""}
                key={index}
              />
            );
          })
        ) : (
          <div className="loader">
            <span></span>
            <span></span>
            <span></span>
          </div>
        )}
      </div>
    </div>
  );
};

export default CoctailsSection;
