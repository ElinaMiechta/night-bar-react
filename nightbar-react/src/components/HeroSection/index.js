import React, { useState } from "react";
import { BiShapeTriangle } from "react-icons/bi";
import { BiDrink } from "react-icons/bi";
import { FcMenu } from "react-icons/fc";
import Nav from "../Nav";
import { Link } from "react-router-dom";

function HeroSection({ imgsrc, title, subtitle }) {
  const [click, setClick] = useState(false);

  const handleClick = () => setClick(!click);

  return (
    <header className="hero-wrapper">
      <div className={click ? "menu-icon active" : "menu-icon"}>
        <Nav handleClick={handleClick} />
      </div>
      <FcMenu
        className={!click ? "menu-bars" : "menu-bars hidden"}
        onClick={handleClick}
      />
      <Link to="/">
        <BiDrink className="menu-logo" />
      </Link>
      <div className="img-wrapper" onClick={handleClick}>
        <img src={imgsrc} alt="bar" />
      </div>

      <div className="hero__content">
        <h1>{title}</h1>
        <h2>{subtitle}</h2>
        <BiShapeTriangle className="hero-logo" />
      </div>
    </header>
  );
}

export default HeroSection;
