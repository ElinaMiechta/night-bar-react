import React from 'react'
import Row from '../Row'
import CardItem from '../CardItem'


const EventSection = () => {
      return (
            <div className="events-container">
                 <Row subTitle={'PARTY HARD'} title={'EVENTS'}/>
                 <div className="cards">
                <CardItem
                imgsrc={'/images/girls.jpg'}
                subtitle={'NOVEMBER 12 2015'}
                title={'Girls night'}
                hrefTxt={'LEARN MORE'} />
                 <CardItem
                imgsrc={'/images/morning.jpg'}
                subtitle={'JUNE 13 2015'}
                title={'Till sun rise'}
                hrefTxt={'LEARN MORE'}  />
                 <CardItem
                imgsrc={'/images/annie.jpg'}
                subtitle={'APRIL 6 2015'}
                title={'Anniversary'} 
                hrefTxt={'LEARN MORE'} />       
            </div>
            </div>
      )
}

export default EventSection
